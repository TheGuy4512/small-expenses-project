#!/usr/bin/python3
import click
from connect import connect, disconnect, init_db
import psycopg
from datetime import date

@click.group()
def main():
    """This application helps to keep expenses accross multiple users on a daily basis.
    For filling up the information, please specify correct command, which is described below."""
    #add('Ivan', 'Milk', 108, '2018-06-07')
    #select('all', None, None, True)
    #clean()
    init_db()

@main.command()
@click.argument('person_name')
@click.argument('name')
@click.argument('value')
@click.argument('exp_date', required=False)
def add(person_name, name, value, exp_date=None):
    """Fills up the information in local db expenses table, as per provided elememts.\n
    PERSON_NAME:    string  Add the name of person with expenses.\n
    NAME:   string  Fill up the name of expense, which is required to calculate.\n
    VALUE:  int  Fill up amout of expense, which correspond to provided name.\n
    EXP_DATE:   date    Write the date, on which you would like to save expenses information. Please, fill up information in YYYY-MM-DD format. By default, it correspnds to the current date."""
    try:
        conn = connect()
    
        cur = conn.cursor()
        cur.execute("SELECT name FROM person;")
        rows = cur.fetchall()
        persons = [person[0] for person in rows]

        if not exp_date:
            exp_date = date.today().isoformat()
        if person_name not in persons:
                cur.execute("INSERT INTO person(name) VALUES (%s);", (person_name,))
        cur.execute("INSERT INTO category(name, value, date, person_id) VALUES (%s, %s, %s, (SELECT id FROM person WHERE name=%s));", (name, value, exp_date, person_name))
            
        conn.commit()
        cur.close()

    except (Exception, psycopg.DatabaseError) as error:
        print(error)
    finally:
        conn = disconnect()

def check_date(row, daily, monthly, yearly):
    if daily and date.today() == row[3]:
        print('user:{0}, expense:{1}, value:{2}, date:{3}'.format(row[0], row[1], row[2], str(row[3])))
    elif monthly and date.today().month == row[3].month and date.today().year == row[3].year:
        print('user:{0}, expense:{1}, value:{2}, date:{3}'.format(row[0], row[1], row[2], str(row[3])))
    elif yearly and date.today().year == row[3].year:
        print('user:{0}, expense:{1}, value:{2}, date:{3}'.format(row[0], row[1], row[2], str(row[3])))
    elif not daily and not monthly and not yearly:
        print('user:{0}, expense:{1}, value:{2}, date:{3}'.format(row[0], row[1], row[2], str(row[3])))

@main.command()
@click.argument('value')
@click.option('--daily', required=False, is_flag=True, help="Display the information of expenses as it is per current day.")
@click.option('--monthly', required=False, is_flag=True, help="Display the information of expenses as it is per current month.")
@click.option('--yearly', required=False, is_flag=True, help="Display the information of expenses as it is per current year.")
def select(value, daily=None, monthly=None, yearly=None):
    """Display the expenses per configured parameters.\n
    VALUE:  string  Show the corresponding expense available in local DB. If the value corresponds to "all" - display all expenses."""
    try:
        conn = connect()
        cur = conn.cursor()
        cur.execute('SELECT person.name, category.name, value, date FROM category JOIN person ON category.person_id=person.id;')
        rows = cur.fetchall()
        if value == 'all':
            for row in rows:
                check_date(row, daily, monthly, yearly)
        else:
            counter = 0
            for row in rows:
                if value == row[1]:
                    check_date(row, daily, monthly, yearly)
                    counter = counter + 1
            if counter == 0:
                print('There is no expense category present in database!')
        
        cur.close()
    except (Exception, psycopg.DatabaseError) as error:
        print(error)
    finally:
        conn = disconnect()

@main.command()
def clean():
    """Clean the current local db instance data."""
    try:
        conn = connect()
        cur = conn.cursor()
        cur.execute('DELETE from category')
        cur.execute('DELETE from person')
        conn.commit()
        print('Cleaning current expenses...')
    except (Exception, psycopg.DatabaseError) as error:
        print(error)
    finally:
        conn = disconnect()

if __name__ == '__main__':
    main().add_command(add)
    main().add_command(select)
    main().add_command(clean)
    main()