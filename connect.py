import psycopg
from config_db import config

conn = None

def connect():
    """ Connect to the PostgreSQL database server """
    
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg.connect(**params)

        return conn
    except (Exception, psycopg.DatabaseError) as error:
        print(error)

def init_db():
    try:
        conn = connect()
        cur = conn.cursor()

        cur.execute("CREATE TABLE IF NOT EXISTS person(id serial, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))")
        cur.execute("CREATE TABLE IF NOT EXISTS category (id serial, name VARCHAR(255) NOT NULL, value INT NOT NULL, date DATE NOT NULL, person_id INT, PRIMARY KEY(id), CONSTRAINT fk_person FOREIGN KEY(person_id) REFERENCES person(id))")

        conn.commit()
    except (Exception, psycopg.DatabaseError) as error:
        print(error)

def disconnect():
    if conn is not None:
        conn.close()